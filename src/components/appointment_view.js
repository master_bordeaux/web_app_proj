/*
  28/09/2021: Thanh Ho added code changes for calling API and performed on layout
  06/10/2021: Thanh Ho added business logic for api of adjusting and deleting
*/

import React, { Component, useContext, useState, useEffect, useRef } from 'react';
import { Link } from 'react-router-dom';
import { Table, Tag, Space, Divider, Input, Button, Popconfirm, Form } from 'antd';
import Banner from '../Home/Banner';

const mapStateToProps = state => ({
  appName: state.common.appName,
});
const url = "http://127.0.0.1:8080/api/appointments";

const EditableContext = React.createContext(null);
const EditableRow = ({ index, ...props }) => {
  const [form] = Form.useForm();
  return (
    <Form form={form} component={false}>
      <EditableContext.Provider value={form}>
        <tr {...props} />
      </EditableContext.Provider>
    </Form>
  );
};
const EditableCell = ({
  title,
  editable,
  children,
  dataIndex,
  record,
  handleSave,
  ...restProps
}) => {
  const [editing, setEditing] = useState(false);
  const inputRef = useRef(null);
  const form = useContext(EditableContext);
  useEffect(() => {
    if (editing) {
      inputRef.current.focus();
    }
  }, [editing]);

  const toggleEdit = () => {
    setEditing(!editing);
    form.setFieldsValue({
      [dataIndex]: record[dataIndex],
    });
  };

  const save = async () => {
    try {
      const values = await form.validateFields();
      toggleEdit();
      handleSave({ ...record, ...values });
    } catch (errInfo) {
      console.log('Save failed:', errInfo);
    }
  };

  let childNode = children;

  if (editable) {
    childNode = editing ? (
      <Form.Item
        style={{
          margin: 0,
        }}
        name={dataIndex}
        rules={[
          {
            required: true,
            message: `${title} is required.`,
          },
        ]}
      >
        <Input ref={inputRef} onPressEnter={save} onBlur={save} />
      </Form.Item>
    ) : (
      <div
        className="editable-cell-value-wrap"
        style={{
          paddingRight: 24,
        }}
        onClick={toggleEdit}
      >
        {children}
      </div>
    );
  }

  return <td {...restProps}>{childNode}</td>;
};

class AppointmentView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: null,
      data_appointment: null
    };

    this.columns_appointment = [
      {
        title: 'ID',
        dataIndex: 'id',
        key: 'id',
      },
      {
        title: 'Service ID',
        dataIndex: 'service_id',
        key: 'service_id',
        editable: true, 
      },
      {
        title: 'Customer ID',
        dataIndex: 'customer_id',
        key: 'customer_id',
        editable: true, 
      },
      {
        title: 'Employee ID',
        dataIndex: 'employee_id',
        key: 'employee_id',
        editable: true, 
      },
      {
        title: 'FromTime',
        key: 'fromTime',
        dataIndex: 'fromTime',
      },
      {
        title: 'ToTime',
        key: 'toTime',
        dataIndex: 'toTime',
      },
      {
        title: 'CreatedAt',
        key: 'createdAt',
        dataIndex: 'createdAt',
      },
      {
        title: 'UpdatedAt',
        key: 'updatedAt',
        dataIndex: 'updatedAt',
      },
      {
        title: 'Amend Operation',
        dataIndex: 'amendOperation',
        render: (_, record) =>
          this.state.data.length >= 1 ? (
            <Popconfirm title="Sure to amend?" onConfirm={() => this.handleAmend(record)}>
              <a>Amend</a>
            </Popconfirm>
          ) : null,
      },
      {
        title: 'Delete Operation',
        dataIndex: 'deleteOperation',
        render: (_, record) =>
          this.state.data.length >= 1 ? (
            <Popconfirm title="Sure to delete?" onConfirm={() => this.handleDelete(record)}>
              <a>Delete</a>
            </Popconfirm>
          ) : null,
      }
    ];
    
    this.columns_service = [
      {
        title: 'ID',
        dataIndex: 'id',
        key: 'id',
      },
      {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
        render: text => <a>{text}</a>,
      },
      {
        title: 'Price',
        dataIndex: 'price',
        key: 'price',
      },
      {
        title: 'CreatedAt',
        key: 'createdAt',
        dataIndex: 'createdAt',
      },
      {
        title: 'UpdatedAt',
        key: 'updatedAt',
        dataIndex: 'updatedAt',
      },
    ];
    
    this.columns_customer = [
      {
        title: 'ID',
        dataIndex: 'id',
        key: 'id',
      },
      {
        title: 'Fullname',
        dataIndex: 'fullName',
        key: 'fullName',
        render: text => <a>{text}</a>,
      },
      {
        title: 'PhoneNumber',
        dataIndex: 'phoneNumber',
        key: 'phoneNumber',
      },
      {
        title: 'CreatedAt',
        key: 'createdAt',
        dataIndex: 'createdAt',
      },
      {
        title: 'UpdatedAt',
        key: 'updatedAt',
        dataIndex: 'updatedAt',
      },
    ];
    
    this.columns_employee = [
      {
        title: 'ID',
        dataIndex: 'id',
        key: 'id',
      },
      {
        title: 'Fullname',
        dataIndex: 'fullName',
        key: 'fullname',
        render: text => <a>{text}</a>,
      },
      {
        title: 'CreatedAt',
        key: 'createdAt',
        dataIndex: 'createdAt',
      },
      {
        title: 'UpdatedAt',
        key: 'updatedAt',
        dataIndex: 'updatedAt',
      },
    ];
  }

  componentDidMount() {
    this.loadPayload();    
  }

  loadPayload = () => {
    const headers = { 'Content-Type': 'application/json' }
    console.log(url, { headers });
    fetch(url, {
      method: 'GET',
      headers: headers
    })
      .then(response => response.json())
      .then(response => {
        console.log(response);        
        let temp_data = [];
        temp_data = response;
        let temp_data_appointment = [];
          
        temp_data.forEach((item) => {
          temp_data_appointment.push({ 
            "id": item.id,
            "service_id": item.service.id,
            "employee_id": item.employee.id,
            "customer_id": item.customer.id,
            "fromTime": item.fromTime,
            "toTime": item.toTime,
            "createdAt": item.createdAt,
            "updatedAt": item.updatedAt
          });
        });  

        this.setState({ 
          data: response,
          data_appointment: temp_data_appointment
        })
      })
      .catch(error => console.log('Failed : ' + error.message));
  }; 

  handleAmend = (record) => {
    let payload = JSON.stringify({
      "id": record.id,
      "serviceId":record.service_id, "customerId":record.customer_id,"employeeId":record.employee_id});
    console.log (payload);
    let self = this;

    fetch(url, {
        method: 'PUT',
        headers: {
            "Content-Type": "application/json"
        },
        body: payload
    })
    .then(response => {
      console.log(response);
      response.json()
    })
    .then(response => {   
        self.loadPayload();       
      })
    .catch(error => {
      console.log('Failed : ' + error.message)      
    });    
  };

  handleDelete = (record) => {
    let payload = JSON.stringify({
      "id": record.id,
      "serviceId":record.service_id, "customerId":record.customer_id,"employeeId":record.employee_id});
    console.log (payload);
    let self = this;

    fetch(url, {
        method: 'DELETE',
        headers: {
            "Content-Type": "application/json"
        },
        body: payload
    })
    .then(response => {
      console.log(response);
      response.json()
    })
    .then(response => {   
        self.loadPayload();  
      })
    .catch(error => {
      console.log('Failed : ' + error.message)      
    });  
  };

  handleSave = (row) => {
    const newData = [...this.state.data_appointment];
    const index = newData.findIndex((item) => row.key === item.key);
    const item = newData[index];
    newData.splice(index, 1, { ...item, ...row });
    this.setState({
      data_appointment: newData,
    });
  };

  render() {
    let temp_data = [];
    temp_data = this.state.data;
    const data_appointment = this.state.data_appointment;
    const data_service = [];
    const data_customer = [];
    const data_employee = [];

    if(temp_data != null) {  
      temp_data.forEach((item) => {        
        data_service.push({
          "id": item.service.id,
          "name": item.service.name,
          "price": item.service.price,
          "createdAt": item.service.price,
          "updatedAt": item.service.updatedAt
        });
        data_customer.push({
          "id": item.customer.id,
          "fullName": item.customer.fullName,
          "phoneNumber": item.customer.phoneNumber,
          "createdAt": item.customer.createdAt,
          "updatedAt": item.customer.updatedAt
        });
        data_employee.push({
          "id": item.employee.id,
          "fullName": item.employee.fullName,
          "createdAt": item.employee.createdAt,
          "updatedAt": item.employee.updatedAt
        });
      });
    }    

    const components = {
      body: {
        row: EditableRow,
        cell: EditableCell,
      },
    };
    const map_columns_appointment = this.columns_appointment.map((col) => {
      if (!col.editable) {
        return col;
      }

      return {
        ...col,
        onCell: (record) => ({
          record,
          editable: col.editable,
          dataIndex: col.dataIndex,
          title: col.title,
          handleSave: this.handleSave,
        }),
      };
    });

    const ids_service = data_service.map(o => o.id)
    const filtered_data_service = data_service.filter(({id}, index) => !ids_service.includes(id, index + 1))

    const ids_customer = data_customer.map(o => o.id)
    const filtered_data_customer = data_customer.filter(({id}, index) => !ids_customer.includes(id, index + 1))

    const ids_employee = data_employee.map(o => o.id)
    const filtered_data_employee = data_employee.filter(({id}, index) => !ids_employee.includes(id, index + 1))

    return (
      <div >
        <Banner appName={this.props.appName} />
        <Divider>
          <h1 style={{ justifyContent: 'center', alignItems: 'center' }}>APPOINTMENTS</h1>
          <p style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Link to="/">
              Home
            </Link>
          </p>
        </Divider>
        <h3 style={{ justifyContent: 'left', alignItems: 'left' }}>APPOINTMENT INFO</h3>
        <Table 
        columns={map_columns_appointment} dataSource={data_appointment} 
        components={components}
        rowClassName={() => 'editable-row'}
        bordered />
        <h3 style={{ justifyContent: 'left', alignItems: 'left' }}>SERVICE INFO</h3>
        <Table columns={this.columns_service} dataSource={filtered_data_service} />
        <h3 style={{ justifyContent: 'left', alignItems: 'left' }}>CUSTOMER INFO</h3>
        <Table columns={this.columns_customer} dataSource={filtered_data_customer} />
        <h3 style={{ justifyContent: 'left', alignItems: 'left' }}>EMPLOYEE INFO</h3>
        <Table columns={this.columns_employee} dataSource={filtered_data_employee} />
      </div>
    );
  };
};
export default AppointmentView;