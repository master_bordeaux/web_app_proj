import React from 'react';
import { Link } from 'react-router-dom';
import { Form, Input, Button, Checkbox, Space, Divider  } from 'antd';
import { Layout, Menu, Breadcrumb } from 'antd';
import Banner from '../Home/Banner';

const mapStateToProps = state => ({
    appName: state.common.appName,
});

const onFinish = (values: any) => {
    console.log('Success:', values);
};

const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
};

class SignedInView extends React.Component {
    render() {
        return (
            <div >
                <Banner appName={this.props.appName} />
                <Divider>
                    <h1 style={{ justifyContent: 'center', alignItems: 'center' }}>Sign In</h1>
                    <p style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Link to="/register">
                            Need an account?
                        </Link>
                    </p>
                </Divider>
                <Form
                    name="basic"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 16 }}
                    initialValues={{ remember: true }}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                >
                    <Form.Item name="test" wrapperCol={{ offset: 8, span: 16 }}>
                    </Form.Item>
                    <Form.Item
                        label="Username"
                        name="username"
                        rules={[{ required: true, message: 'Please input your username!' }]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="Password"
                        name="password"
                        rules={[{ required: true, message: 'Please input your password!' }]}
                    >
                        <Input.Password />
                    </Form.Item>

                    <Form.Item name="remember" valuePropName="checked" wrapperCol={{ offset: 8, span: 16 }}>
                        <Checkbox>Remember me</Checkbox>
                    </Form.Item>

                    <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                        <Button type="primary" htmlType="submit">
                            Sign in
                        </Button>
                    </Form.Item>
                </Form>
            </div>
        );
    };
};

export default SignedInView;