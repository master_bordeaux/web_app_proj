/*
  28/09/2021: Thanh Ho added code changes for calling API and performed on layout
  06/10/2021: Thanh Ho added business logic for api of adjusting and deleting
*/

import React, { Component, useContext, useState, useEffect, useRef } from 'react';
import { Link } from 'react-router-dom';
import { Table, Tag, Space, Divider, Input, Button, Popconfirm, Form } from 'antd';
import Banner from '../Home/Banner';

const mapStateToProps = state => ({
  appName: state.common.appName,
});
const url = "http://127.0.0.1:8080/api/customers";

const EditableContext = React.createContext(null);
const EditableRow = ({ index, ...props }) => {
  const [form] = Form.useForm();
  return (
    <Form form={form} component={false}>
      <EditableContext.Provider value={form}>
        <tr {...props} />
      </EditableContext.Provider>
    </Form>
  );
};
const EditableCell = ({
  title,
  editable,
  children,
  dataIndex,
  record,
  handleSave,
  ...restProps
}) => {
  const [editing, setEditing] = useState(false);
  const inputRef = useRef(null);
  const form = useContext(EditableContext);
  useEffect(() => {
    if (editing) {
      inputRef.current.focus();
    }
  }, [editing]);

  const toggleEdit = () => {
    setEditing(!editing);
    form.setFieldsValue({
      [dataIndex]: record[dataIndex],
    });
  };

  const save = async () => {
    try {
      const values = await form.validateFields();
      toggleEdit();
      handleSave({ ...record, ...values });
    } catch (errInfo) {
      console.log('Save failed:', errInfo);
    }
  };

  let childNode = children;

  if (editable) {
    childNode = editing ? (
      <Form.Item
        style={{
          margin: 0,
        }}
        name={dataIndex}
        rules={[
          {
            required: true,
            message: `${title} is required.`,
          },
        ]}
      >
        <Input ref={inputRef} onPressEnter={save} onBlur={save} />
      </Form.Item>
    ) : (
      <div
        className="editable-cell-value-wrap"
        style={{
          paddingRight: 24,
        }}
        onClick={toggleEdit}
      >
        {children}
      </div>
    );
  }

  return <td {...restProps}>{childNode}</td>;
};

class CustomerView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: null
    };

    this.columns = [
      {
        title: 'ID',
        dataIndex: 'id',
        key: 'id',
      },
      {
        title: 'Fullname',
        dataIndex: 'fullName',
        key: 'fullname',
        render: text => <a>{text}</a>,
        editable: true,
      },
      {
        title: 'PhoneNumber',
        dataIndex: 'phoneNumber',
        key: 'phoneNumber',
        editable: true,
      },
      {
        title: 'CreatedAt',
        key: 'createdAt',
        dataIndex: 'createdAt',
      },
      {
        title: 'UpdatedAt',
        key: 'updatedAt',
        dataIndex: 'updatedAt',
      },
      {
        title: 'Amend Operation',
        dataIndex: 'amendOperation',
        render: (_, record) =>
          this.state.data.length >= 1 ? (
            <Popconfirm title="Sure to amend?" onConfirm={() => this.handleAmend(record)}>
              <a>Amend</a>
            </Popconfirm>
          ) : null,
      },
      {
        title: 'Delete Operation',
        dataIndex: 'deleteOperation',
        render: (_, record) =>
          this.state.data.length >= 1 ? (
            <Popconfirm title="Sure to delete?" onConfirm={() => this.handleDelete(record)}>
              <a>Delete</a>
            </Popconfirm>
          ) : null,
      }
    ];
  }

  componentDidMount() {
    this.loadPayload();
  }

  loadPayload = () => {
    const headers = { 'Content-Type': 'application/json' }
    fetch(url, {
      method: 'GET',
      headers: headers
    })
      .then(response => response.json())
      .then(response => {
        console.log(response);
        this.setState({ data: response})
      })
      .catch(error => console.log('Failed : ' + error.message));
  }; 
  
  handleAmend = (record) => {
    let payload = JSON.stringify({"id": record.id, "name":record.fullName, "phone":record.phoneNumber});
    console.log (payload);
    let self = this;

    fetch(url, {
        method: 'PUT',
        headers: {
            "Content-Type": "application/json"
        },
        body: payload
    })
    .then(response => {
      console.log(response);
      response.json()
    })
    .then(response => {   
        self.loadPayload();       
      })
    .catch(error => {
      console.log('Failed : ' + error.message)      
    });    
  };

  handleDelete = (record) => {
    let payload = JSON.stringify({"id": record.id, "name": record.fullName});
    console.log (payload);
    let self = this;

    fetch(url, {
        method: 'DELETE',
        headers: {
            "Content-Type": "application/json"
        },
        body: payload
    })
    .then(response => {
      console.log(response);
      response.json()
    })
    .then(response => {   
        self.loadPayload();  
      })
    .catch(error => {
      console.log('Failed : ' + error.message)      
    });  
  };

  handleSave = (row) => {
    const newData = [...this.state.data];
    const index = newData.findIndex((item) => row.key === item.key);
    const item = newData[index];
    newData.splice(index, 1, { ...item, ...row });
    this.setState({
      data: newData,
    });
  };

  render() {
    const data = this.state.data;
    const components = {
      body: {
        row: EditableRow,
        cell: EditableCell,
      },
    };
    const map_columns = this.columns.map((col) => {
      if (!col.editable) {
        return col;
      }

      return {
        ...col,
        onCell: (record) => ({
          record,
          editable: col.editable,
          dataIndex: col.dataIndex,
          title: col.title,
          handleSave: this.handleSave,
        }),
      };
    });

    return (
      <div >
        <Banner appName={this.props.appName} />
        <Divider>
          <h1 style={{ justifyContent: 'center', alignItems: 'center' }}>CUSTOMERS</h1>
          <p style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Link to="/">
              Home
            </Link>
          </p>
        </Divider>
        <Table 
        columns={map_columns} 
        dataSource={data} 
        components={components}
        rowClassName={() => 'editable-row'}
        bordered /> 
      </div>
    );
  };
};
export default CustomerView;