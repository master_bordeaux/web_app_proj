import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Table, Tag, Space, Divider } from 'antd';
import Banner from '../Home/Banner';

const mapStateToProps = state => ({
  appName: state.common.appName,
});


// ReactDOM.render(<Table columns={columns} dataSource={data} />, mountNode);

class PaymentView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: null
    };
  }

  render() {
    const { data } = this.state;
    console.log(data);
    return (
      <div >
        <Banner appName={this.props.appName} />
        <Divider>
          <h1 style={{ justifyContent: 'center', alignItems: 'center' }}>PAYMENT</h1>
          <p style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Link to="/">
              Home
            </Link>
          </p>
        </Divider>
      </div>
    );
  };
};
export default PaymentView;