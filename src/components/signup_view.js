import React from 'react';
import { Link } from 'react-router-dom';
import { Form, Input, Button, Checkbox, Space, Divider  } from 'antd';
import { Layout, Menu, Breadcrumb } from 'antd';
import Banner from '../Home/Banner';

const mapStateToProps = state => ({
    appName: state.common.appName,
});

const onFinish = (values: any) => {
    console.log('Success:', values);
};

const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
};

class SignedUpView extends React.Component {
    render() {
        return (
            <div >
                <Banner appName={this.props.appName} />
                <Divider>
                    <h1 >Sign Up</h1>
                    <p >
                        <Link to="/login">
                            Have an account?
                        </Link>
                    </p>
                </Divider>
                <Form
                    name="basic"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 16 }}
                    initialValues={{ remember: true }}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                >
                    <Form.Item
                        label="Username"
                        name="username"
                        rules={[{ required: true, message: 'Please input your username!' }]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="Email"
                        name="email"
                        rules={[{ required: true, message: 'Please input your email!' }]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="Password"
                        name="password"
                        rules={[{ required: true, message: 'Please input your password!' }]}
                    >
                        <Input.Password />
                    </Form.Item>

                    <Form.Item
                        label="Retype Password"
                        name="Retype password"
                        rules={[{ required: true, message: 'Please re-input your password!' }]}
                    >
                        <Input.Password />
                    </Form.Item>

                    <Form.Item name="agreement" valuePropName="checked" wrapperCol={{ offset: 8, span: 16 }}>
                        <Checkbox>Agreement</Checkbox>
                    </Form.Item>

                    <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                        <Button type="primary" htmlType="submit">
                            Sign up
                        </Button>
                    </Form.Item>
                </Form>
            </div>
        );
    };
};

export default SignedUpView;