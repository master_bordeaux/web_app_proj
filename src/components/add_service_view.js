/*
  28/09/2021: Thanh Ho added code changes for calling API
*/

import React from 'react';
import { Link } from 'react-router-dom';
import { Form, Input, Button, Checkbox, Space, Divider  } from 'antd';
import { Layout, Menu, Breadcrumb } from 'antd';
import Banner from '../Home/Banner';

const mapStateToProps = state => ({
    appName: state.common.appName,
});

const onFinish = (values: any) => {
    console.log('Success:', values);
    let payload = JSON.stringify({"name": values.service, "price": values.price});
    console.log (payload);
    fetch('http://127.0.0.1:8080/api/services', {
        method: 'POST',
        headers: {
            "Content-Type": "application/json"
        },
        body: payload
    })
    .then(function(response) {
        return response.json();
      })
    .then(function(data) {
        console.log(data);
      })
    .catch(error => console.log('Failed : ' + error.message));
};

const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
};

class AddServiceView extends React.Component {
    render() {
        return (
            <div >
                <Banner appName={this.props.appName} />
                <Divider>
                    <h1 style={{ justifyContent: 'center', alignItems: 'center' }}>NEW SERVICE</h1>
                    <p style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Link to="/">
                            HOME
                        </Link>
                    </p>
                </Divider>
                <Form
                    name="basic"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 16 }}
                    initialValues={{ remember: true }}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                >
                    <Form.Item name="test" wrapperCol={{ offset: 8, span: 16 }}>
                    </Form.Item>
                    <Form.Item
                        label="Service"
                        name="service"
                        rules={[{ required: true, message: 'Please input service name!' }]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="Price"
                        name="price"
                        rules={[{ required: true, message: 'Please input price of service!' }]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                        <Button type="primary" htmlType="submit" >
                            Create
                        </Button>
                    </Form.Item>
                </Form>
            </div>
        );
    };
};

export default AddServiceView;