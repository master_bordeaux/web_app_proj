// import logo from './logo.svg';
import './App.css';
import React, { Component } from 'react'
import { Route, Switch } from 'react-router-dom';
import Home from './Home';
import SignedInView from './components/signin_view'
import SignedUpView from './components/signup_view'
import AppointmentView from './components/appointment_view'
import ServicesView from './components/services_view'
import CustomerView from './components/customer_view'
import PaymentView from './components/payment_view'
import StaffView from './components/staff_view'
import AddServiceView from './components/add_service_view'
import AddAppointmentView from './components/add_appointment_view'
import AddCustomerView from './components/add_customer_view'
import AddEmployeeView from './components/add_employee_view'

const mapStateToProps = state => {
  return {
    appLoaded: state.common.appLoaded,
    appName: state.common.appName,
    currentUser: state.common.currentUser,
    redirectTo: state.common.redirectTo
  }
};

class App extends Component {

  render() {

    return (
      <div>
        {/* <Header appName={this.props.appName} currentUser={this.props.currentUser} /> */}
        <Switch>
          <Route exact path="/" >
            <Home appName={this.props.appName}/>
          </Route>
          <Route path="/login" > 
            <SignedInView appName={this.props.appName}/>
          </Route>
          <Route path="/register" > 
            <SignedUpView appName={this.props.appName}/>
          </Route>
          <Route path="/appointments" > 
            <AppointmentView appName={this.props.appName}/>
          </Route>
          <Route path="/addAppointment" > 
            <AddAppointmentView appName={this.props.appName}/>
          </Route>
          <Route path="/services" > 
            <ServicesView appName={this.props.appName}/>
          </Route>
          <Route path="/addService" > 
            <AddServiceView appName={this.props.appName}/>
          </Route>
          <Route path="/customers" > 
            <CustomerView appName={this.props.appName}/>
          </Route>
          <Route path="/addCustomer" > 
            <AddCustomerView appName={this.props.appName}/>
          </Route>
          <Route path="/payments" > 
            <PaymentView appName={this.props.appName}/>
          </Route>
          <Route path="/staffs" > 
            <StaffView appName={this.props.appName}/>
          </Route>
          <Route path="/addStaff" > 
            <AddEmployeeView appName={this.props.appName}/>
          </Route>
        </Switch>
      </div>
    )
  }
}

export default App;
