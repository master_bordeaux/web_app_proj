// import './home.css'
import React from 'react';
import { Layout, Menu, Breadcrumb, Divider } from 'antd';
import { 
  UserOutlined, 
  UsergroupAddOutlined, 
  FolderAddOutlined, 
  NotificationOutlined, 
  CalendarOutlined, 
  CustomerServiceOutlined,
  LaptopOutlined,
  PayCircleOutlined,
} from '@ant-design/icons';

import { Link } from 'react-router-dom';
const { SubMenu } = Menu;
const { Header, Content, Sider, Footer } = Layout;

const mapStateToProps = state => ({
  token: state.common.token,
  appName: state.common.appName,
  content: state.common.content,
});

class MainView extends React.Component {
  render() {
    return (
      <Layout>
        <Header className="header">
          <div className="logo" />
          <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['2']}>
            <Menu.Item key="1">Home
              <Link to="/" ></Link>
            </Menu.Item>
            <Menu.Item key="2">Sign in
              <Link to="/login" ></Link>
            </Menu.Item>
            <Menu.Item key="3">Sign up
              <Link to="/login" ></Link>
            </Menu.Item>
            <Menu.Item key="2">Contact</Menu.Item>
          </Menu>
        </Header>
        <Layout>
          <Sider width={200} className="site-layout-background">
            <Menu
              mode="inline"
              defaultSelectedKeys={['1']}
              defaultOpenKeys={['sub1']}
              style={{ height: '100%', borderRight: 0 }}
            >
              <SubMenu key="sub1" icon={<CalendarOutlined />} title="Appointments">
                <Menu.Item icon={<FolderAddOutlined />}>New Appointment<Link to="/addAppointment" ></Link></Menu.Item>
                <Menu.Item >Show all<Link to="/appointments" ></Link></Menu.Item>
              </SubMenu>
              <SubMenu key="sub2" icon={<LaptopOutlined />} title="Services">
                <Menu.Item icon={<FolderAddOutlined />}>New Service<Link to="/addService" ></Link></Menu.Item>
                <Menu.Item >Show all<Link to="/services" ></Link></Menu.Item>
              </SubMenu>
              <SubMenu key="sub3" icon={<UsergroupAddOutlined />} title="Customers">
                <Menu.Item  icon={<FolderAddOutlined />}>New customer<Link to="/addCustomer" ></Link></Menu.Item>
                <Menu.Item >Show all<Link to="/customers" ></Link></Menu.Item>
              </SubMenu>
              <SubMenu key="sub4" icon={<PayCircleOutlined />} title="Payments">
                <Menu.Item  icon={<FolderAddOutlined />}>New invoice</Menu.Item>
                <Menu.Item >Show all<Link to="/payments" ></Link></Menu.Item>
              </SubMenu>
              <SubMenu key="sub5" icon={<UserOutlined />} title="Staffs">
                <Menu.Item  icon={<FolderAddOutlined />}>New staff<Link to="/addStaff" ></Link></Menu.Item>
                <Menu.Item >Show all<Link to="/staffs" ></Link></Menu.Item>
              </SubMenu>
            </Menu>
          </Sider>
          <Layout style={{ padding: '0 24px 24px' }}>
            <Breadcrumb style={{ margin: '16px 0' }}>
              <Breadcrumb.Item>Home</Breadcrumb.Item>
              <Breadcrumb.Item>List</Breadcrumb.Item>
              <Breadcrumb.Item>App</Breadcrumb.Item>
            </Breadcrumb>
            <Content
              className="site-layout-background"
              style={{
                padding: 24,
                margin: 0,
                minHeight: 280,
              }}
            >
              <div>HOME CONTENT</div>
            </Content>
            <Footer style={{ textAlign: 'center' }}>Service Management App ©2020 Created by IEI Team
            </Footer>
          </Layout>
        </Layout>
      </Layout>
    );
  };
}

export default MainView;
