import Banner from './Banner';
import MainView from './MainView';
import React from 'react';
import Clock from '../components/Clock';
import { Divider } from 'antd';
import './home.css'

const mapStateToProps = state => ({
  ...state.home,
  appName: state.common.appName,
  token: state.common.token
});

const HomeContent = () => {
  // return (
  //   <Divider orientation="left">Left Text</Divider>
  // )
  return 'HOME CONTENT'
}

class Home extends React.Component {

  render() {
    return (
      <div>
        <Banner appName={this.props.appName} />
        <MainView appName={this.props.appName} content={HomeContent}/>
        {/* <Clock /> */}
      </div>
    );
  }
}

export default Home;
