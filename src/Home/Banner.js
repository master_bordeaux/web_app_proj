import React from 'react';
import { Link } from 'react-router-dom';
const Banner = ({ appName}) => {
  return (
    <div className="banner">
      <div className="container">
        <h1 className="logo-font">
          <Link to="/" >{appName}</Link>
        </h1>
        <p>A place to manage your service.</p>
      </div>
    </div>
  );
};

export default Banner;
